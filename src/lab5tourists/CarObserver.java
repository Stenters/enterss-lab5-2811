/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab4: Bee Decorator
 * Author:     Stuart Enters
 * Date:       1/16/2019
 */

package lab5tourists;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import lab5tourists.mobileEntities.Car;
import lab5tourists.mobileEntities.MobileEntity;
import lab5tourists.mobileEntities.Person;

import java.util.ArrayList;

/**
 * Class to observe cars
 *
 * @author Stuart Enters
 * @version 1.0 created on 1/16/2019 at 10:38
 */
public class CarObserver {


    private final CityMap map;
    private ArrayList<Character> letters;
    private ArrayList<Character> busLetters;

    public CarObserver(CityMap map){
        this.map = map;

        this.letters = new ArrayList<>();
        letters.add('*');
        letters.add('*');
        letters.add('*');
        letters.add('*');

        this.busLetters = new ArrayList<>();
        busLetters.add('*');
        busLetters.add('*');
        busLetters.add('*');

    }

    /**
     * Method for handling a car getting tagged
     * @param tagged the entity tagged
     * @param tagger the entity tagging
     */
    public void notifyObserver(MobileEntity tagged, MobileEntity tagger){
        if (tagged.getClass() == Car.class && tagger.getClass() == Person.class){       // Check if tagged is a car
                                                                                        // and tagger is the player
            // Check if the car has any target letters in its license plate
            getTargetLetters((Car) tagged);

            // Get the canvas to write to, then clear it
            var canvas = (Canvas) map.getChallengeNodes().get(0);
            var context = canvas.getGraphicsContext2D();
            clearContext(context);

            // Record how many letters have been found
            context.strokeText("Challenge: Find all the letters in MSOE\n" +
                    "Goal: MSOE\nFound: " + letters.toString(),0,20);

            if (!letters.contains('*')) {       // Check if all letters have been found

                // Record that the MSOE challenge is complete
                clearContext(context);
                context.strokeText("Challenge: Find all the letters in MSOE\n" +
                        "Goal: MSOE\nCHALLENGE COMPLETED",0,20);
            }

            if (map.getChallengeNodes().size() > 2){        // Check if a bus has been tagged

                // Get the canvas to write to, then clear it
                var busCanvas = (Canvas) map.getChallengeNodes().get(2);
                var busContext = busCanvas.getGraphicsContext2D();
                clearContext(busContext);

                // Record how many letters have been found
                busContext.strokeText("Challenge: Find all the letters in BUS\n" +
                        "Goal: BUS\nFound: " + busLetters.toString(),0,20);

                if(!busLetters.contains('*')) {     // Check if all letters have been found

                    // Record that teh BUS challenge is complete
                    clearContext(busContext);
                    busContext.strokeText("Challenge: Find all the letters in BUS\n" +
                            "Goal: BUS\nCHALLENGE COMPLETED",0,20);

                }
            }
        }
    }

    /**
     * Method for clearing the canvas
     * @param context the context of the canvas
     */
    private void clearContext(GraphicsContext context) {
        // Call clearRect with the proper dimensions
        context.clearRect(0, 0, 250, 100);
    }

    /**
     * Helper method for checking if the car had any target letters
     * @param tagged the car that was tagged
     */
    private void getTargetLetters(Car tagged) {
        var plate = tagged.getPlate();  // Get the license plate of the car

        // Check for MSOE
        checkLetters(plate, 'M', 0, letters);
        checkLetters(plate, 'S', 1, letters);
        checkLetters(plate, 'O', 2, letters);
        checkLetters(plate, 'E', 3, letters);

        if (map.getChallengeNodes().size() > 2){        // Check if a bus has been tagged

            // Check for BUS
            checkLetters(plate, 'B', 0, busLetters);
            checkLetters(plate, 'U', 1, busLetters);
            checkLetters(plate, 'S', 2, busLetters);
        }
    }

    /**
     * Helper method for checking if a letter hasn't been found
     * @param plate the plate (possibly) containing the letter
     * @param letter the letter to check for
     * @param index the index to write to
     * @param array the array to write to
     */
    private void checkLetters(String plate, char letter, int index, ArrayList<Character> array) {
        if (plate.indexOf(letter) > -1 && array.get(index) == '*'){     // Check that the plate has the letter
                                                                        // and the array doesn't
            // Replace the '*' with the letter
            array.remove(index);
            array.add(index, letter);
        }
    }
}