/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab4: Bee Decorator
 * Author:     Stuart Enters
 * Date:       2/5/2019
 */

package lab5tourists;

/**
 * Factory for mass producing entities
 *
 * @author Stuart Enters
 * @version 1.0 created on 2/5/2019 at 11:22
 */
public abstract class EntityFactory {

    protected final CityMap map;

    EntityFactory(CityMap map){
        this.map = map;
    }

    public abstract void spawn(int number);
}