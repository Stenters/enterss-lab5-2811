/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab4: Bee Decorator
 * Author:     Stuart Enters
 * Date:       1/16/2019
 */

package lab5tourists;

import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import lab5tourists.mobileEntities.MobileEntity;
import lab5tourists.mobileEntities.Person;

import java.util.ArrayList;

/**
 * Class for observing the museum
 *
 * @author Stuart Enters
 * @version 1.0 created on 1/16/2019 at 10:44
 */
public class MuseumObserver {

    private final CityMap map;
    private int imgCounter;
    private ArrayList<Museum> tagged;

    protected MuseumObserver(CityMap map) {
        this.map = map;
        this.imgCounter = 0;
        this.tagged = new ArrayList<>();
    }

    /**
     * Method for handling the museum getting tagged
     * @param museum the museum that got tagged
     * @param entity the entity that tagged the museum
     */
    public void notifyObserver(Museum museum, MobileEntity entity) {
        if (!tagged.contains(museum) && entity.getClass() == Person.class) {        // Check if the museum hasn't been tagged before
                                                                                    // and the player is tagging it
            // Draw a new art piece to the museumChallenge canvas
            var canvas = (Canvas) map.getChallengeNodes().get(1);
            var context = canvas.getGraphicsContext2D();
            var image = new Image("file:\\\\\\" + System.getProperty("user.dir") + "\\src\\lab5tourists\\img\\wood-gatherer.png");

            // Offset the image by the number of images drawn
            context.drawImage(image, 0,  50 + imgCounter * 40, 80, 80);
            imgCounter++;

            // Record that the museum has been tagged
            tagged.add(museum);
        }
    }
}