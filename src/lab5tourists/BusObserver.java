/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab4: Bee Decorator
 * Author:     Stuart Enters
 * Date:       1/16/2019
 */

package lab5tourists;


import javafx.scene.canvas.Canvas;
import lab5tourists.mobileEntities.MobileEntity;
import lab5tourists.mobileEntities.Person;

/**
 * Class for observing if a bus is hit
 *
 * @author Stuart Enters
 * @version 1.0 created on 1/16/2019 at 10:18
 */
public class BusObserver {

    private final CityMap map;

    public BusObserver(CityMap map){
        this.map = map;
    }

    /**
     * Method for handling a bus getting tagged
     */
    public void notifyObserver(MobileEntity entity){
        if (entity.getClass() == Person.class) {        // Check if the entity is the player
            if (map.getChallengeNodes().size() < 3) {   // Check if a bus has already been tagged

                // If a bus hasn't been tagged, reveal the secret challenge
                Canvas busChallenge = new Canvas(250, 452);
                busChallenge.getGraphicsContext2D().strokeText("Challenge: Find all the letters in BUS\n" +
                        "Goal: BUS\nFound: ***", 0, 20);
                map.addChallengeNode(busChallenge);
            }
        }
    }
}