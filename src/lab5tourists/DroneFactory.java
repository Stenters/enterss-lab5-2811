/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab4: Bee Decorator
 * Author:     Stuart Enters
 * Date:       2/7/2019
 */

package lab5tourists;

import lab5tourists.mobileEntities.Drone;

/**
 * Factory for mass producing Drones
 *
 * @author Stuart Enters
 * @version 1.0 created on 2/7/2019 at 13:38
 */
public class DroneFactory extends EntityFactory {

    public DroneFactory(CityMap map){
        super(map);
    }

    @Override
    public void spawn(int number) {
        while (number-- > 0){
            new Drone(map);
        }

    }
}