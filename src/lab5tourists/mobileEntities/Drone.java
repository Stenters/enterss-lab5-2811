/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 5: Tourists
 * Author:     Dr. Yoder and YOUR NAME HERE
 * Date:       13 Jan 2019
 */
package lab5tourists.mobileEntities;

import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import lab5tourists.CityMap;

/**
 * A randomly-moving drone
 *
 * Finds a point, moves toward that
 *
 */
public class Drone extends MobileEntity {
    private Point2D goal;
    public static final int DRONE_MAX_SPEED = 1;
    private static final Image DRONE_IMAGE =
            new Image(CityMap.class.getResource("img/drone.png").toString());


    /**
     * Create a new car with a unique license plate number
     * @param cityMap The map to which this car will be added.
     */
    public Drone(CityMap cityMap) {
        super(cityMap, DRONE_IMAGE);
        setName("Drone "+ MobileEntity.instanceCount + ": " + hashCode());
    }

    /**
     * Steps as all MobileEntities do.
     *
     * Calculates a point, then moves toward that
     */
    @Override
    protected void step() {
        if (goal == null){
            goal = chooseRandomInBoundsPoint();
        }

        Point2D direction = goal.subtract(getLocation());
        if(Math.hypot(direction.getX(), direction.getY()) < DRONE_MAX_SPEED) {
            // last step to goal
            stepSize = direction;
            goal = null;        // Signify getting a new goal
        } else {
            // approaching goal
            stepSize = direction.multiply(DRONE_MAX_SPEED /
                    Math.hypot(direction.getX(), direction.getY()));
        }
        super.step();
    }

    /**
     * This method is called when this mobile entity is tagged by another entity.
     *
     * @param entity The entity that tagged this mobile entity.
     */
    @Override
    public void taggedBy(MobileEntity entity) {
        // Do nothing
    }

}
